// Generated by CoffeeScript 1.12.4
(function() {
  if (delta.en == null) {
    delta.en = {};
  }

  delta.en.constants = {
    "flight_search_form": {
      "return_tab": "ROUND-TRIP",
      "one_way_tab": "ONE-WAY",
      "show_price_in_label": "SHOW PRICE IN",
      "basic_economy": "Basic Economy",
      "main_cabin": "Main Cabin",
      "delta_comfort_plus": "Delta Comfort+&reg;",
      "first_class": "First Class",
      "delta_premium": "Premium Select",
      "delta_one": "Delta One&reg;",
      "refundable_fares_only": "Refundable Fares only",
      "city_or_airport_placeholder": "Enter City or Airport Code",
      "airport_code": "Airport Code",
      "passengers": "PASSENGERS"
    },
    "flight_search_results": {
      "show_price_in_label": "SHOW PRICE IN",
      "layover_label": "Layover",
      "details_label": "View Details",
      "seat_map_link_label": "View Seats",
      "not_offered": "Not Offered",
      "sold_out": "Sold Out",
      "compare_experience_label": "Compare Experiences",
      "seats_remaining_label": "{seats} left at this price"
    },
    "flight_details_form": {
      "taxes_and_fees": "LEARN MORE ABOUT TAXES/FEES",
      "equipment": "Equipment",
      "aircraft": "Aircraft",
      "meal_service": "MEAL SERVICE",
      "full_details": "FULL DETAILS"
    },
    "flight_specific_product_modal": {
      "flight_number_label": "Flight Number",
      "miles_flown": "Miles Flown",
      "performance_for": "Performance for",
      "from": "FROM",
      "select": "Select"
    },
    "seat_map": {
      "see_seat_key": "See Seat Key",
      "exit_row_qualification": {
        "title": "Exit Row Qualification",
        "regulation_criteria": "Federal regulations require that passengers meet a set of criteria in order to be seated in an exit.",
        "meet_all_criteria_button": "Yes, I Meet All Requirements"
      }
    },
    "view_more": "VIEW MORE",
    "class": "Class",
    "flight_summary": {
      "trip_summary": "Trip Summary",
      "review_message1": "Please review your selected flights.",
      "taxes_and_fees": "includes taxes & fees",
      "taxes_and_charge": "TAXES, FEES AND CHARGES",
      "review_and_purchase": "Review Passenger Info",
      "search": "Modify Search",
      "upsell_dialog_text": "Enhance Your Experience",
      "fare_conditions": "Change & Cancellation policies",
      "show_not_enough_miles_msg": "There are not enough miles in your skymiles account to redeem this award ticket, visit our",
      "cuba_dialog_title": "You have selected a flight to Cuba",
      "buy_and_transfer_miles": "buygftxfer"
    },
    "passenger_info": {
      "choose_from_saved_companions": "CHOOSE FROM SAVED COMPANIONS",
      "sfpd_mismatch": "The companion's SFPD information has changed, since you initially saved as companion. Please enter the correct passenger information for this booking. To permanently update the companion record, please navigate to My Profile on delta.com.",
      "add_passenger": "Add Passenger Info",
      "message": "PLEASE MAKE SURE YOUR FULL NAME IS ENTERED EXACTLY AS IT APPEARS ON YOUR GOVERNMENT-ISSUED IDENTIFICATION",
      "example_text": "(Example: Jane Elizabeth Doe)",
      "prefix": "Prefix (optional)",
      "skymiles_number": "Add SkyMiles Number",
      "skymiles": "SkyMiles # (OPTIONAL)",
      "frequent_flyer_hash": "Frequent Flyer #",
      "frequent_flyer_program": "Frequent Flyer Program",
      "frequent_flyer_number": "Frequent Flyer Number",
      "frequent_flyer": "Frequent Flyer # (OPTIONAL)",
      "redress_placeholder": "Redress # (OPTIONAL)",
      "secure_flight_info": "For additional information on how to make updates to your Secure Flight Information, please see My Profile on delta.com",
      "secure_flight_passenger": "Important: If your Secure Flight passenger name is incorrect, you can change it in the Passenger Info section above.",
      "redress": "IF YOU HAVE A TSA-ISSUED REDRESS NUMBER OR KNOWN TRAVELER NUMBER ENTER IT HERE",
      "known_placeholder": "Known Traveler # (OPTIONAL)",
      "contact_note": "In case we need to contact you about this transaction.",
      "device_type": "DEVICE TYPE",
      "cell_placeholder": "Cell",
      "phone": "Phone",
      "email": "Email",
      "confirmation_message": "Purchase confirmation will be sent to this address.",
      "sky_bonus_message": "My company is a <b>Delta SkyBonus<sup>&reg;</sup></b> or<br/> <b>Air France/KLM BlueBiz<sup>&reg;</sup></b> program member."
    },
    "booking_confirmation": {
      "booking_confirmation": "Purchase Confirmation",
      "confirmation": "Confirmation #:",
      "baggage_fees": "Baggage Fees",
      "ticket_number": "Ticket #:",
      "view_my_trip": "View My Trip",
      "miles_available": "MILES AVAILABLE",
      "join_skymiles": "JOIN SKYMILES",
      "enrollment_banner_chiclet_header": "Don't leave miles behind",
      "enrollment_banner_chiclet_header_with_miles": "Don't leave these miles behind",
      "enrollment_banner_message": "Create your free Delta SkyMiles® account and earn miles after your trip that you can use for flights, upgrades and more. With SkyMiles, your miles don't expire and there are no blackout dates on Delta Air Lines.",
      "enrollment_banner_message_with_miles": "Create your free Delta SkyMiles® account and earn {miles} miles after your trip that you can use for flights, upgrades and more. With SkyMiles, your miles don't expire and there are no blackout dates on Delta Air Lines.",
      "baggage_allowance": {
        "no_currency_code_indicator": "*"
      }
    },
    "payment_info": {
      "add_a_payment_method": "Add A Payment Method",
      "cc_dc_type": "Credit/Debit Card Type",
      "expiration_month": "Expiration Month",
      "expiration_year": "Expiration Year",
      "state": "State",
      "postal_code": "Postal Code",
      "store_my_card_label": "STORE MY CARD & USE FOR EXPRESS CHECKOUT",
      "cc_choose_card_type": "Please enter one of the following credit card types:",
      "complete_purchase": "COMPLETE PURCHASE"
    },
    "review_and_purchase": {
      "note": "Please review your information before purchasing.",
      "taxes_fees_charges": "Taxes, Fees and Charges",
      "seat_upgrade_and_seat_extras": "Seat Upgrade & Trip Extras",
      "fare_conditions": "Change & Cancellation policies",
      "terms_and_conditons_sub_header": "Purchase of this ticket means you agree to the Terms and Conditions below and the Hazardous Materials Policy."
    },
    "seats_trip_extras": {
      "heading": "Seats & Trip Extras",
      "message_1": "Make your trip as comfortable as possible."
    },
    "quick_flight_status": {
      "check_flight_status": "CHECK FLIGHT STATUS",
      "status_by": "Flight #"
    },
    "flight_status": {
      "flight_date": "Flight Date",
      "view_button": "View Results",
      "city": "City"
    },
    "flight_schedule_results": {
      "departs_label": "Departs",
      "arrives_label": "Arrives"
    },
    "flight_status_details": {
      "flight_label": "Flight#",
      "total_time": "TOTAL TIME",
      "flight_time": "FLIGHT TIME",
      "aircraft": "AIRCRAFT"
    },
    "checkin": {
      "verified": "VERIFIED BY DELTA",
      "passport_info_description": "Enter passport information exactly as it appears on your passport. Verification of travel documents is still required at the airport. You can insert your passport at an airport kiosk or present your documentation to a Delta agent.",
      "passport_information": "Passport information is required for international travel. Please complete the passport information for each passenger to continue",
      "no_passport_info_message": "I DON'T HAVE INFORMATION FOR THIS PASSENGER.",
      "passenger_removal_warning": "(Selecting this option will remove the passenger from online checkin at this time.)",
      "passenger_with_associated_infant_removal_warning": "(Selecting this option will remove the passenger and infant from the online check-in at this time.)",
      "expiration_date": "EXPIRATION DATE",
      "add_emergency_contact": "ADD AN EMERGENCY CONTACT",
      "emergency_contact_warning": "The emergency contact for each passenger cannot be travelling on your flight(s)",
      "first_name": "Contact first name (given name)",
      "last_name": "Contact last name (surname)",
      "contact_number": "Phone number (include area code)",
      "passport_details_updated": "YOUR PASSPORT INFORMATION HAS BEEN SUCCESSFULLY UPDATED.",
      "visa_requirement_warning": "The following passengers require a visa for this trip. Please ensure they have the required documentation before going to the airport.",
      "document_message": "At the airport, present your documentation to a Delta agent.",
      "view_visa_pages": "VIEW VISA AND PASSPORT INFORMATION >",
      "select_bags": "Select Bags",
      "skip_select_bags_message": "How many bags are you checking, excluding carry-ons? Or skip this step and check bags at the airport.",
      "baggage_fee": "BAGGAGE FEE",
      "additional_charges": "Bags will be weighed and measured when you drop them off at the airport. Additional charges may apply for overweight or oversized bags. Once payment is processed you cannot change your bags online.",
      "form_of_payment": "FORM OF PAYMENT",
      "first_name_on_card": "First Name On Card",
      "last_name_on_card": "Last Name On Card",
      "expiry_date": "EXPIRATION MONTH/YEAR",
      "complete_purchase": "COMPLETE PURCHASE",
      "acknowledge_button": "Acknowledge And Continue",
      "special_item_bag": "SPECIAL ITEMS",
      "special_item_bag_sticker": "ANY OF THESE BAGS",
      "depart": "DEPART",
      "arrive": "ARRIVE",
      "pending_bags": "Pending Bags",
      "flight_confirmation": "Flight confirmation #",
      "return_date_description": "Based on your itinerary, we cannot determine your eligibility to travel and remain at your destination. To complete check-In, please enter the date you will be returning and then click CONTINUE",
      "return_date_label": "Return date",
      "pending_bags_label": "PENDING BAGS",
      "bags_footer_note": "Bags will be weighed and measured when you drop them off at the airport. Additional charges may apply for overweight or oversized bags.",
      "upgrade_button_title": "Continue to Check In",
      "email_receipt_failure_msg": "We are unable to request a receipt for this payment. Please check with a Delta agent at the airport.",
      "infant_in_arms": "Infant in Arms",
      "infant_extra_baggage": "Please call Delta Reservation at <a href='tel:1-888-750-3284'>1-888-750-3284</a> or see an agent for infant baggage.",
      "upgrade_title": "Upgrade Fees",
      "upgrade_details": "Details",
      "no_email_purchase_confirmation": ""
    },
    "fare_rule_policies": {
      "fare_rule_summary": "Fare Rule Summary"
    },
    "promo": {
      "app_name": "Fly Delta",
      "airlines_name": "Delta Air Lines, Inc."
    },
    "tax_breakdown_per_pax_dialog": {
      "air_transportation_charges": "Air Transportation Charges",
      "base_fare": "Base Fare*",
      "international_surcharge": "Carrier-imposed International Surcharge*",
      "cash_per_passenger": "Cash per Passenger**",
      "non_refundable_note": "**Nonrefundable."
    },
    "security_code_help_dialog": {
      "title": "Credit Card ID"
    },
    "fare_change_dialog": {
      "para1_line1": "We’re sorry, but the previously quoted price is no longer available. The new price is",
      "para1_line2": "(per passenger). To continue, please verify your payment information or ",
      "para1_line3_link": "begin a new search."
    },
    "fare_sold_out_dialog": {
      "button_info": "Tap Start Over to return to the Reservations input screen and select different dates and times."
    },
    "search_results_expired_dialog": {
      "title": "Your Flight Results Have Expired",
      "details": "Your flight results have expired due to inactivity. We need to check again for up-to-the-minute rates and availability. Please refresh your results or start a new search."
    },
    "home": {
      "view_site": "VIEW FULL HTML SITE"
    },
    "login": {
      "placeholderText": "SkyMiles® Number or Username"
    },
    "select_seat_heading": "Seats & Trip Extras",
    "seat_upgrade_and_seat_extras": "Seat Upgrade & Trip Extras",
    "passenger_data_heading": "Secure Flight Passenger Data",
    "upgrade_request": "Upgrade Request",
    "upgrade_request_note": "Eligible members may enjoy complimentary upgrades when available.",
    "upgrade_request_first": "Request upgrade for First/Business class",
    "upgrade_request_save": "SAVE PREFERENCES TO MY PROFILE",
    "upgrade_request_comfort_plus": "Request upgrade for Delta Comfort+™",
    "upgrade_request_comfort_plus_reg": "Request upgrade for Delta Comfort+®",
    "upgrade_request_comfort_plus_note": "Aisle, middle or window seats will be assigned based on availability.",
    "continue_to_purchase": "Review & Purchase",
    "seat_cost_label": "Total for all passengers",
    "global_controls": {
      "connection_airport_info": "Providing a connection airport will limit your results to flights connecting in the specified airport within the maximum connection times."
    },
    "special_items": {
      "info": "Each special item you're checking in counts as one bag. We don’t need to know what it is, just that you have one and how many bags in total you're checking in. If there are any additional charges we'll let you know at the airport.",
      "bags_weight": "Bags weighing more than 50 lbs",
      "oversized_bags": "OVERSIZED BAGS<br>(EXCLUDING GOLF CLUBS)",
      "bags_dimension_details": "Bags over 62 inches when you total length + width + girth",
      "child_seats": "Stroller, child restraint seats",
      "wheelchair": "Wheelchair",
      "sporting_eqiupment": "SPORTING EQUIPMENT<br>(EXCLUDING GOLF CLUBS)",
      "sporting_eqipments_examples": "Bicycle; bowling, fishing, or ski equipment; parachute; surf or windsurf boards; SCUBA tank"
    },
    "boarding_documents": {
      "phone_number": "Area/City code and phone number",
      "email_address": "MOBILE EMAIL ADDRESS",
      "enter_email_address": "Email Address",
      "boarding_pass_later_message": "JUST CHECK ME IN. I WILL GET MY BOARDING PASS LATER.",
      "from_the_app": "From the Fly Delta App",
      "print_later": "Print later on delta.com",
      "print_at_kiosk": "Print at airport kiosk",
      "complete_checkin": "COMPLETE CHECK IN",
      "complete_checkin_note": "Complete check in and get your boarding documents.",
      "options": "BOARDING DOCUMENT OPTIONS",
      "email_option": "MOBILE EMAIL ADDRESS",
      "phone_option": "MOBILE PHONE NUMBER",
      "selection_note": "Select the eBoarding option below and provide your mobile information. When you arrive at the airport, show your eBoarding Pass to TSA Security and the gate agent for scanning."
    },
    "pnr_search_result": {
      "departs": "DEPARTS",
      "arrives": "ARRIVES",
      "ticket_number": "TICKET #",
      "skymiles_number": "FREQUENT FLYER #",
      "number": "#"
    },
    "checkin_complete": {
      "arrival_time_message": "GET TO THE AIRPORT ON TIME",
      "arrival_time_recommendation": "The recommended arrival time prior to departure is 2 hours for Domestic and 3 hours for International.",
      "drop_your_bag": "DROP YOUR BAG",
      "bags_drop_note": "Bags will be weighed and measured when you drop them off at the airport. Additional charges may apply for overweight or oversized bags.",
      "proceed_to_security": "PROCEED TO SECURITY",
      "proceed_note": "For flights within the United States, you must be on board the aircraft at least 15 minutes prior to departure, so make sure to plan accordingly.",
      "flight_number_label": "FLIGHT#"
    },
    "baggage_payment": {
      "confirmation_note": "Thank you, your purchase is complete. An email confirmation has been sent to ",
      "tapping_confirm": "By tapping confirm payment, "
    },
    "passport_alerts": {
      "customer_advisory": "Customer Advisory",
      "passport_expiry": "THE FOLLOWING PASSPORT WILL EXPIRE BEFORE THIS ITINERARY IS COMPLETE.",
      "contact_local_office": "Please contact your local reservation office for assistance.",
      "view_visa": "VIEW VISA AND PASSPORT INFORMATION >",
      "file_esta": "File ESTA Now >",
      "checkin_not_done": "Sorry, your check-in request cannot be completed.",
      "travellers_entering_US": "Travelers entering the U.S. under the Visa Waiver Program are now required to have a travel authorization (ESTA) on file with the U.S. Government.",
      "check_status": "You will need to request or check the status of your travel authorization.",
      "return_to_checkin": "Once the ESTA is on file, you can return to complete check-in.",
      "checkin_at_airport": "Certain documents, such as a U.S. resident card may not require an ESTA to be filed. If you hold a resident card or re-entry document for the U.S. and ESTA is not required, you will need to check-in at the airport.",
      "travellers_entering_US_EVUS": "Travelers entering the U.S. are required to have valid EVUS authorization on file with U.S. Customs.",
      "valid_EVUS_authorization": "If you are traveling on a People’s Republic of China (PRC) Passport and have a valid 10 year B1, B2 or B1/B2 visa you are required to have a valid EVUS authorization. If you do not have EVUS authorization you can apply here: ",
      "checkin_us_card_info": "If you are a U.S. Permanent Resident, you can attempt the check-in process by ensuring you provide your U.S. Resident card information during the check-in process."
    },
    "efirst": {
      "enhance_your_experience": "ENHANCE YOUR EXPERIENCE",
      "efirst_subtitle": "Make your trip as comfortable as possible",
      "purchase_summary": "Purchase summary will be available at the end of check-in as a receipt for this charge",
      "for_all_passengers": "for all passengers",
      "no_thanks_button": "NO THANKS, CONTINUE CHECKING IN",
      "pay_now_button": "PAY FOR SELECTION NOW"
    },
    "credit_card_presentation": {
      "subtitle": "One time use credit card numbers provided by some credit card companies cannot be used at delta.com."
    }
  };

}).call(this);
