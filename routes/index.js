var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

/* Mongo test insert and show results. */
router.get('/insert', function(req, res, next) {
  var MongoClient = require('mongodb').MongoClient, 
      format = require('util').format;
 
  MongoClient.connect('mongodb://127.0.0.1:27017/test', function(err, db) {
    if(err) throw err;
 
    var collection = db.collection('test_insert');
    collection.insert({ name: 'Delta' }, function(err, docs) {
      
      collection.count(function(err, count) {
        // console.log(format("count = %s", count));
      });
 
      // Locate all the entries using find 
      collection.find({ name: 'Delta' }).map(function(doc){
        return doc.name;
      }).toArray(function(err, results) {
        // Render view
        res.render('insert', { results: results.join('<br>') });
        // Let's close the db 
        db.close();
      });
    });
  });
});

module.exports = router;
